---
layout: markdown_page
title: "Dev Backend Department"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Teams

There are a number of teams within the Dev Backend department:

* [Create](/handbook/engineering/dev-backend/create/)
* [Distribution](/handbook/engineering/dev-backend/distribution/)
* [Geo](/handbook/engineering/dev-backend/geo/)
* [Gitaly](/handbook/engineering/dev-backend/gitaly/)
* [Gitter](/handbook/engineering/dev-backend/gitter/)
* [Manage](/handbook/engineering/dev-backend/manage/)
* [Plan](/handbook/engineering/dev-backend/plan/)

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)

## Staff meeting

Managers in the Dev Backend department have a regularly scheduled staff meeting.
This meeting is on the GitLab Availability calendar and the agenda is public
inside GitLab. Other team members may join or be invited as necessary.

In the interest of time, all agenda matters should be discussed asynchronously
when possible. If there are no agenda items that need live discussion, the
meeting will be canceled for the week.
