---
layout: markdown_page
title: "No Ticket Provisioning"
---
## Tickets Slow Down Delivery
Traditional enterprise approaches expect  IT Ops to manage and operate routine work for the organization.  Typically the process to get things done requires the administrative overhead of 'IT tickets' to request and perform repetitive tasks such as requesting infrastructure, configuring an application or provisioning a test environment.   The ticket mindset introduces complexity and delays in provisioning, deployment, scaling, and other software delivery and management activities.  

**In simple terms:  tickets = slow.**

In the world of DevOps, where speed of delivery is critical, **automation** and **self-service** are keys to success.

## GitLab Enalbes Self Service
As a single application designed to support the entire DevOps lifecycle, GitLab enables teams to eliminate unnecessary tickets, and unleash the power of self service:

- Automatically provision and deploy applications
- Provision and manage containers
- Manage Kubernetes 
- Instrument and enable application performance monitoring
